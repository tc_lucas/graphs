package Grafo;

import java.util.*;

public class DirectedLabeledGraphMatrix
{
	private class Vertice {
		private String item;
		public Vertice(String item){
  		  this.item = item;
		}
		public String getItem() {
		  return this.item;
		}
		public void setItem(String item) {
		  this.item = item;
		}
	}

	private int max;
	private int[][] matrix;
	private ArrayList<Vertice> vert;

	public DirectedLabeledGraphMatrix(int n)
	{
		if (n <= 0)
			throw new IllegalArgumentException("Numero de nodos invalido!");

		max = n;
		matrix = new int[max][max];
		vert = new ArrayList<Vertice>(max);
		// inicializacao da matriz
		for (int i = 0; i < max; i++)
			for (int j = 0; j < max; j++)
				matrix[i][j] = 0;
	}

	private int indexItem(String item)
	{
		int i, res = -1;
	    // percorre a lista de vertices para achar o elemento
	    for (i=0; ((i < vert.size()) && !item.equals(vert.get(i).getItem())); i++);

	    // se o indice em ‘i’ for válido, retorna-o
	    if (i < vert.size())
	    	res = i;

	    return res;
	}

	public void addVertice(String item)
	{
		// se tem espaco pra adicionar ainda
		if (vert.size() < max)
		{
			// se o vertice ainda nao foi adicionado, entao adiciona-o
			if (indexItem(item) == -1)
			{
				Vertice v = new Vertice(item);
				vert.add(v);				
			}
		}
		else
			throw new IllegalArgumentException("Capacidade do grafo atingida: " + max);
	}

	public void addEdge(String strOrig, String strDest, int weight)
	{
		int orig, dest;

		orig = indexItem(strOrig);
		dest = indexItem(strDest);

		if (orig == -1)
			throw new IllegalArgumentException("Aresta origem invalida: " + strOrig);
		else if (dest == -1)
			throw new IllegalArgumentException("Aresta destino invalida: " + strDest);
		else
		{
			matrix[orig][dest] = weight;
		}
	}

	private String indice2name(int i)
	{
		if (i < max)
			return vert.get(i).getItem();
		return null;
	}

	public int getNumVertices()
	{
		return max;
	}

	public int getDegree(String vertice)
	{
		int c = 0;
		int idx = indexItem(vertice);
		if (idx != -1)
		{
			// grau de saida
			for (int j = 0; j < max; j++)
				if (matrix[idx][j] != 0) c++;
			// grau de entrada
			for (int i = 0; i < max; i++)
				if (matrix[i][idx] != 0) c++;
			
		}
		return c;
	}

	public ArrayList<String> getAllAdjacents(String vertice)
	{
		ArrayList<String> res = null;
		int idx = indexItem(vertice);
		if (idx != -1)
		{
			res = new ArrayList<String>();
			for (int j = 0; j < max; j++)
				if (matrix[idx][j] != 0)
					res.add(vert.get(j).getItem());
		}
		return res;
	}

	public ArrayList<String> getSources()
	{
		ArrayList<String> res = new ArrayList<String>();
		boolean checked;

		for (int i=0; i<max; i++)
		{
			checked = false;
			for (int j=0; j<max; j++)
			{
				if (matrix[j][i] != 0)
				{
					checked = true;
					break;
				}
			}

			if (!checked)
				res.add(indice2name(i));
		}
		return res;
	}

	public ArrayList<String> getSinks()
	{
		ArrayList<String> res = new ArrayList<String>();
		boolean checked;
		for (int i=0; i<max; i++)
		{
			checked = false;
			for (int j=0; j<max; j++)
				if (matrix[i][j] != 0)
				{
					checked = true;
					break;
				}

			if (!checked)
				res.add(indice2name(i));
		}
		return res;
	}

	public void showInfo()
	{
		System.out.print("V = { ");
		for (int i = 0; i < max-1; i++)
			System.out.printf("%s, ", indice2name(i));
		System.out.printf("%s }\n", indice2name(max-1));

		ArrayList<String> arestas = new ArrayList<String>();
		for (int i = 0; i < max; i++)
			for (int j = 0; j < max; j++)
				if (matrix[i][j] != 0)
					arestas.add(String.format("(%s, %s, %d)", indice2name(i), indice2name(j), matrix[i][j]));

		System.out.print("E = {\n");
		if (!arestas.isEmpty())
		{
			System.out.printf("      %s", arestas.get(0));

			for (int i = 1; i < arestas.size(); i++)
				System.out.printf(",\n      %s", arestas.get(i));
		}
		System.out.println("\n    }");
	}

	public void showMatrix()
	{
		int i;
		System.out.printf("       ");
		for (i = 0; i < max; i++)
			System.out.printf("%5s", indice2name(i));
		for (i = 0; i < max; i++)
		{
			System.out.printf("\n%5s: ", indice2name(i));
			for (int j = 0; j < max; j++)
				System.out.printf("%5d", matrix[i][j]);
		}
		System.out.println();
	}

	public static void main(String[] args)
	{
		DirectedLabeledGraphMatrix g = new DirectedLabeledGraphMatrix(5);

		g.addVertice("A");
		g.addVertice("B");
		g.addVertice("C");
		g.addVertice("D");
		g.addVertice("E");

		g.addEdge("A", "B", 3);
		g.addEdge("A", "C", 4);
		g.addEdge("B", "D", 5);
		g.addEdge("C", "D", 6);
		g.addEdge("D", "E", 7);
		g.addEdge("E", "A", 8);

		System.out.println("\nGrafo: ");
		g.showInfo();

		System.out.println("\nMatriz: ");
		g.showMatrix();
	
		ArrayList<String> s = g.getSources();
		if (!s.isEmpty())
		{
			System.out.printf("\nSources: %s", s.get(0));
			for (int i = 1; i < s.size(); i++)
				System.out.printf(", %s", s.get(i));
		}
		System.out.println("");

		s = g.getSinks();
		if (!s.isEmpty())
		{
			System.out.printf("\nSinks: %s", s.get(0));
			for (int i = 1; i < s.size(); i++)
				System.out.printf(", %s", s.get(i));
		}
		System.out.println("");

		System.out.println("\nGrau(A): " + g.getDegree("A"));
		System.out.println("Grau(E): " + g.getDegree("E"));

		ArrayList<String> adj = g.getAllAdjacents("A");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(A): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
		
		adj = g.getAllAdjacents("C");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(C): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
	}
}
