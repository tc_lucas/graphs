package Grafo;

import java.util.*;

public class UndirectedGraphList
{
	private class Vertice
	{
		private String item;
		private ArrayList<Vertice> lst = new ArrayList<Vertice>();

		public Vertice(String item) {
			this.item = item;
		}
		public String getItem() {
			return this.item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public void addAdjacent(Vertice v) {
			if (!lst.contains(v))
				lst.add(v);
		}
		public ArrayList<Vertice> getAdjacents() {
			return lst;
		}
		public Vertice getAdjacent(int i) {
			Vertice res = null;
			if ((i >= 0) && (i < lst.size()))
				res = lst.get(i);
			return res;
		}
		public int getDegree(){
			return lst.size(); 
		}
	
	}

	private ArrayList<Vertice> vert;

	public UndirectedGraphList() {
		vert = new ArrayList<Vertice>();
	}

	private Vertice searchVerticeRef(String item)
	{
		Vertice res = null;
		int i;

		for (i = 0; ((i < vert.size()) && !vert.get(i).getItem().equals(item)); i++);

		if (i < vert.size())
			res = vert.get(i);

		return res;
	}

	public void addVertice(String item)
	{
		if (searchVerticeRef(item) == null) 
		{
			Vertice novo = new Vertice(item);
			vert.add(novo);
		}
	}

	public void addEdge(String strOrig, String strDest)
	{
		Vertice vAux1 = searchVerticeRef(strOrig);
		Vertice vAux2 = searchVerticeRef(strDest);

		if (vAux1 == null)
			throw new IllegalArgumentException("Aresta origem invalida: " + strOrig);
		else if (vAux2 == null)
			throw new IllegalArgumentException("Aresta destino invalida: " + strDest);
		else
		{
			vAux1.addAdjacent(vAux2);
			vAux2.addAdjacent(vAux1);
		}
	}

	public int getNumVertices() {
		return vert.size();
	}

	public int getDegree(String vertice)
	{
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
			return v.getDegree();
		return 0;
	}

	public ArrayList<String> getAllAdjacents(String vertice)
	{
		ArrayList<String> res = null;
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
		{
			res = new ArrayList<String>();
			for (int j = 0; j < v.getDegree(); j++)
				res.add(v.getAdjacent(j).getItem());
		}
		return res;
	}

	public void showInfo()
	{
		System.out.print("V = { ");
		for (int i = 0; i < vert.size()-1; i++)
			System.out.printf("%s, ", vert.get(i).getItem());
		System.out.printf("%s }\n", vert.get(vert.size()-1).getItem());

		ArrayList<String> arestas = new ArrayList<String>();
		for (int i = 0; i < vert.size(); i++)
			for (int j = 0; j < vert.get(i).lst.size(); j++)
				arestas.add(String.format("(%s, %s)", vert.get(i).getItem(), vert.get(i).getAdjacent(j).getItem()));

		System.out.print("E = {\n");
		if (!arestas.isEmpty()) {
			System.out.printf("      %s", arestas.get(0));

			for (int i = 1; i < arestas.size(); i++)
				System.out.printf(",\n      %s", arestas.get(i));
		}
		System.out.println("\n    }");
	}

	public static void main(String[] args)
	{
		UndirectedGraphList g = new UndirectedGraphList();

		g.addVertice("A");
		g.addVertice("B");
		g.addVertice("C");
		g.addVertice("D");
		g.addVertice("E");

		g.addEdge("A", "B");
		g.addEdge("A", "C");
		g.addEdge("B", "D");
		g.addEdge("C", "D");
		g.addEdge("D", "E");
		g.addEdge("E", "A");

		System.out.println("\nGrafo: ");
		g.showInfo();

		System.out.println("\nGrau(A): " + g.getDegree("A"));
		System.out.println("Grau(E): " + g.getDegree("E"));

		ArrayList<String> adj = g.getAllAdjacents("A");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(A): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
		
		adj = g.getAllAdjacents("C");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(C): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
	}
}
