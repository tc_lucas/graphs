package Grafo;

import java.util.*;

public class UndirectedLabeledGraphList
{
	private class Edge
	{
		private Vertice v;
		private int w; // weight
		
		public Edge()
		{
			v = null;
			w = 0;
		}
		public Edge(Vertice vert, int weight)
		{
			v = vert;
			w = weight;
		}
		public Vertice getVertice() {
			return v;
		}
		public void setVertice(Vertice elem) {
			this.v = elem;
		}
		public int getWeight() {
			return w;
		}
		public void setWeight(int weight) {
			this.w = weight;
		}
	}

	private class Vertice
	{
		private String item;
		private ArrayList<Edge> lst = new ArrayList<Edge>();

		public Vertice(String item) {
			this.item = item;
		}
		public String getItem() {
			return this.item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public boolean equals(Object o)
		{
			if (o instanceof Vertice)
			{
				return ((Vertice)o).getItem().equals(this.getItem());
			}
			return false;
		}
		private Edge searchEdgeRef(Vertice v, int w)
		{
			Edge res = null;
			int i;

			for (i = 0; ((i < lst.size()) && !lst.get(i).getVertice().equals(v)); i++);

			if (i < lst.size())
				res = lst.get(i);

			return res;
		}
		public void addAdjacent(Vertice v, int w)
		{
			Edge eAux = searchEdgeRef(v, w); 
			if (eAux == null)
			{
				// se nao existe ainda, entao adiciona a lista
				Edge e = new Edge(v, w);
				lst.add(e);
			}
			else
			{
				// se ja existe, entao apenas atualiza o peso
				eAux.setWeight(w);			
			}
		}
		public ArrayList<Edge> getAdjacents() {
			return lst;
		}
		public Edge getAdjacent(int i) {
			Edge res = null;
			if ((i >= 0) && (i < lst.size()))
				res = lst.get(i);
			return res;
		}
		public int getDegree(){
			return lst.size(); 
		}
	}

	private ArrayList<Vertice> vert;

	public UndirectedLabeledGraphList() {
		vert = new ArrayList<Vertice>();
	}

	private Vertice searchVerticeRef(String item)
	{
		Vertice res = null;
		int i;

		for (i = 0; ((i < vert.size()) && !vert.get(i).getItem().equals(item)); i++);

		if (i < vert.size())
			res = vert.get(i);

		return res;
	}

	public void addVertice(String item)
	{
		if (searchVerticeRef(item) == null) 
		{
			Vertice novo = new Vertice(item);
			vert.add(novo);
		}
	}

	public void addEdge(String strOrig, String strDest, int weight)
	{
		Vertice vAux1 = searchVerticeRef(strOrig);
		Vertice vAux2 = searchVerticeRef(strDest);

		if (vAux1 == null)
			throw new IllegalArgumentException("Aresta origem invalida: " + strOrig);
		else if (vAux2 == null)
			throw new IllegalArgumentException("Aresta destino invalida: " + strDest);
		else
		{
			vAux1.addAdjacent(vAux2, weight);
			vAux2.addAdjacent(vAux1, weight);
		}
	}

	public int getNumVertices() {
		return vert.size();
	}

	public int getDegree(String vertice)
	{
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
			return v.getDegree();
		return 0;
	}

	public ArrayList<String> getAllAdjacents(String vertice)
	{
		ArrayList<String> res = null;
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
		{
			res = new ArrayList<String>();
			for (int j = 0; j < v.getDegree(); j++)
				res.add(v.getAdjacent(j).getVertice().getItem());
		}
		return res;
	}

	public void showInfo()
	{
		System.out.print("V = { ");
		for (int i = 0; i < vert.size()-1; i++)
			System.out.printf("%s, ", vert.get(i).getItem());
		System.out.printf("%s }\n", vert.get(vert.size()-1).getItem());

		ArrayList<String> arestas = new ArrayList<String>();
		for (int i = 0; i < vert.size(); i++)
			for (int j = 0; j < vert.get(i).lst.size(); j++)
				arestas.add(String.format("(%s, %s, %d)", vert.get(i).getItem(), vert.get(i).getAdjacent(j).getVertice().getItem(), vert.get(i).getAdjacent(j).getWeight()));

		System.out.print("E = {\n");
		if (!arestas.isEmpty()) {
			System.out.printf("      %s", arestas.get(0));

			for (int i = 1; i < arestas.size(); i++)
				System.out.printf(",\n      %s", arestas.get(i));
		}
		System.out.println("\n    }");
	}

	public static void main(String[] args)
	{
		UndirectedLabeledGraphList g = new UndirectedLabeledGraphList();

		g.addVertice("A");
		g.addVertice("B");
		g.addVertice("C");
		g.addVertice("D");
		g.addVertice("E");

		g.addEdge("A", "B", 3);
		g.addEdge("A", "C", 4);
		g.addEdge("B", "D", 5);
		g.addEdge("C", "D", 6);
		g.addEdge("D", "E", 7);
		g.addEdge("E", "A", 8);

		System.out.println("\nGrafo: ");
		g.showInfo();

		System.out.println("\nGrau(A): " + g.getDegree("A"));
		System.out.println("Grau(E): " + g.getDegree("E"));

		ArrayList<String> adj = g.getAllAdjacents("A");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(A): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
		
		adj = g.getAllAdjacents("C");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(C): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
	}
}
