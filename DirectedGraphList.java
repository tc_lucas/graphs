import java.util.*;

public class DirectedGraphList
{
	private class Vertice
	{
		private String item;
		private ArrayList<Vertice> lst = new ArrayList<Vertice>();
		private boolean Visited = false;

		public Vertice(String item) {
			this.item = item;
		}
		public String getItem() {
			return this.item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public void addAdjacent(Vertice v) {
			if (!lst.contains(v))
				lst.add(v);
		}
		public ArrayList<Vertice> getAdjacents() {
			return lst;
		}
		public Vertice getAdjacent(int i) {
			Vertice res = null;
			if ((i >= 0) && (i < lst.size()))
				res = lst.get(i);
			return res;
		}
		public int getDegree(){
			return lst.size(); 
		}
	}

	private ArrayList<Vertice> vert;

	public DirectedGraphList() {
		vert = new ArrayList<Vertice>();
	}

	private Vertice searchVerticeRef(String item)
	{
		Vertice res = null;
		int i;

		for (i = 0; ((i < vert.size()) && !vert.get(i).getItem().equals(item)); i++);

		if (i < vert.size())
			res = vert.get(i);

		return res;
	}

	public void addVertice(String item)
	{
		if (searchVerticeRef(item) == null) 
		{
			Vertice novo = new Vertice(item);
			vert.add(novo);
		}
	}

	public void addEdge(String strOrig, String strDest)
	{
		Vertice vAux1 = searchVerticeRef(strOrig);
		Vertice vAux2 = searchVerticeRef(strDest);

		if (vAux1 == null)
			throw new IllegalArgumentException("Aresta origem invalida: " + strOrig);
		else if (vAux2 == null)
			throw new IllegalArgumentException("Aresta destino invalida: " + strDest);
		else
		{
			vAux1.addAdjacent(vAux2);
		}
	}

	public int getNumVertices() {
		return vert.size();
	}

	public int getDegree(String vertice)
	{
		int i, j, c = 0;
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
		{
			// grau de saida
			c += v.getDegree();

			// grau de entrada
			for (i = 0; i < vert.size(); i++)
			{
				if (!vert.get(i).getItem().equals(vertice))
				{
					for (j = 0; j < vert.get(i).getDegree(); j++)
					{
						if (vert.get(i).getAdjacent(j).getItem().equals(vertice))
							c++;
					}					
				}
			}
		}
		return c;
	}

	public ArrayList<String> getAllAdjacents(String vertice)
	{
		ArrayList<String> res = null;
		Vertice v = searchVerticeRef(vertice);
		if (v != null)
		{
			res = new ArrayList<String>();
			for (int j = 0; j < v.getDegree(); j++)
				res.add(v.getAdjacent(j).getItem());
		}
		return res;
	}

	public ArrayList<String> getSources()
	{
		ArrayList<String> res = new ArrayList<String>();
		boolean checked;
		String vertice;

		for (int k=0; k<vert.size(); k++)
		{
			vertice = vert.get(k).getItem();

			checked = false;
			for (int i=0; i<vert.size(); i++)
			{
				for (int j=0; j<vert.get(i).getDegree(); j++)
				{
					if (vert.get(i).getAdjacent(j).getItem().equals(vertice))
					{
						checked = true;
						break;
					}
				}
			}

			if (!checked)
				res.add(vertice);
		}
		return res;
	}

	public ArrayList<String> getSinks()
	{
		ArrayList<String> res = new ArrayList<String>();

		for (int i=0; i<vert.size(); i++)
		{
			if (vert.get(i).getAdjacents().isEmpty())
				res.add(vert.get(i).getItem());
		}
		return res;
	}

	public void showInfo()
	{
		System.out.print("V = { ");
		for (int i = 0; i < vert.size()-1; i++)
			System.out.printf("%s, ", vert.get(i).getItem());
		System.out.printf("%s }\n", vert.get(vert.size()-1).getItem());

		ArrayList<String> arestas = new ArrayList<String>();
		for (int i = 0; i < vert.size(); i++)
			for (int j = 0; j < vert.get(i).lst.size(); j++)
				arestas.add(String.format("(%s, %s)", vert.get(i).getItem(), vert.get(i).getAdjacent(j).getItem()));

		System.out.print("E = {\n");
		if (!arestas.isEmpty()) {
			System.out.printf("      %s", arestas.get(0));

			for (int i = 1; i < arestas.size(); i++)
				System.out.printf(",\n      %s", arestas.get(i));
		}
		System.out.println("\n    }");
	}

	public static void main(String[] args)
	{
		DirectedGraphList g = new DirectedGraphList();

		g.addVertice("A");
		g.addVertice("B");
		g.addVertice("C");
		g.addVertice("D");
		g.addVertice("E");

		g.addEdge("A", "B");
		g.addEdge("A", "C");
		g.addEdge("B", "D");
		g.addEdge("C", "D");
		g.addEdge("D", "E");
		g.addEdge("E", "A");

		System.out.println("\nGrafo: ");
		g.showInfo();

		ArrayList<String> s = g.getSources();
		if (!s.isEmpty())
		{
			System.out.printf("\nSources: %s", s.get(0));
			for (int i = 1; i < s.size(); i++)
				System.out.printf(", %s", s.get(i));
		}
		System.out.println("");

		s = g.getSinks();
		if (!s.isEmpty())
		{
			System.out.printf("\nSinks: %s", s.get(0));
			for (int i = 1; i < s.size(); i++)
				System.out.printf(", %s", s.get(i));
		}
		System.out.println("");

		System.out.println("\nGrau(A): " + g.getDegree("A"));
		System.out.println("Grau(E): " + g.getDegree("E"));

		ArrayList<String> adj = g.getAllAdjacents("A");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(A): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
		
		adj = g.getAllAdjacents("C");		
		if (!adj.isEmpty())
		{
			System.out.printf("Adjacentes(C): %s", adj.get(0));
			for (int i = 1; i < adj.size(); i++)
				System.out.printf(", %s", adj.get(i));
		}
		System.out.println("");
		ArrayList<String> lista = new ArrayList<String>();
		System.out.println("\nAmigos dos amigos de A" + getReachableNodesFromX(2,g.searchVerticeRef("A"),lista));
	}
	
	public static ArrayList<String> getReachableNodesFromX(int distance, Vertice v,ArrayList<String> response){
			for(Vertice ver : v.getAdjacents()){
				if (ver.Visited == false && !response.contains(ver.item)){
					ver.Visited = true;
					response.add(ver.item);
					getReachableNodesFromX(distance--,ver,response);
				}				
			}
		return response;
	}

}


















